#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : cosmic_intruders.sh                                           ##
##  Project   : projects_builder                                              ##
##  Date      : Jun 16, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2020                                                ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Import                                                                     ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
./scripts/build.sh --mode release --platform desktop --dist

## @TODO(stdmatt): Currently don't have a proper GNU/Linux installation and
## the WSL doesn't builds the emscripten stuff correctly...
OS_NAME="$(pw_os_get_simple_name)";
if [ "$OS_NAME" == "$(PW_OS_OSX)" ]; then
    ./scripts/build.sh --mode release --platform web --dist
fi;
