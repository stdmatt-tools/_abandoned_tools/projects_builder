#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : cosmic_intruders.sh                                           ##
##  Project   : projects_builder                                              ##
##  Date      : Jun 16, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2020                                                ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

## XXX(stdmatt): We need to find a better way to grap exactly the last
## built directory and copy its contents, not this hacky way that
## we're doing now.
VERSION=$(echo "$PROJECT_TAG" | sed s/v//g);
cp -rv "./build/cosmic-intruders_web_$VERSION"/* "$PROJECT_OUTPUT_PATH/latest";